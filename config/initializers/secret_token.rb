# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Colchonet::Application.config.secret_key_base = 'f3f5fa7eb93b61f68ebf0e31bff5d4ae3c87d2d98b8c27687acad3cd86e637d79bd2e0aad6ebfa76252aeb777d3634c86bfc419c4104fb2b27a4381219be9b9f'
