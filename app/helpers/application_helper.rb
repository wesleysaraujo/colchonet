module ApplicationHelper
  def error_tag(model, attribute)
    if model.errors.has_key? attribute
      content_tag(
        :div,
        model.errors[attribute].first,
        class: 'error_message'
      )
    end
  end
  
  def nav_link(link_text, link_path)
    class_name = current_page?(link_path) ? 'ativo' : ''
    link_to link_text, link_path, :class => class_name
  end
end
