#encoding: utf-8
class User < ActiveRecord::Base  
  EMAIL_REGEXP = /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  
  has_many :rooms
  
  scope :most_recent, -> { order('created_at DESC') }
  scope :from_rio, -> { where(location: 'Rio de Janeiro') }
  #scope :confirmed, -> { where.not(confirmed_at: nil) }
  scope :confirmed, -> { where(confirmed_at: nil) }
  
	validates_presence_of :email, :full_name, :location
	#validates_confirmation_of :password
	validates_length_of :bio, minimum: 30, allow_blank: false
	validates_uniqueness_of :email
	validate :email_format
  
  has_secure_password
  
  before_create do |user|
    user.confirmation_token = SecureRandom.urlsafe_base64
  end
  
  def confirm!
    return if confirmed?
    
    self.confirmed_at = Time.current
    self.confirmation_token = ''
    save!
  end
  
  def confirmed?
    confirmed_at.present?
  end
	
  private
	# Essa validaçãp pode ser representada da seguinte forma:
	# vlaidate_format_of :email, with: EMAIL_REGEXP

	def email_format
		errors.add(:email, :invalid) unless email.match(EMAIL_REGEXP)		
	end
  
  def self.authenticate(email, password)
    user = confirmed.find_by(email: email).try(:authenticate, password)
  end
end
